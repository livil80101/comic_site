<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/11/22
 * Time: 7:14 PM
 */
Route::get('test', "DataController@test")->name("ajx.test");
Route::post('test', "DataController@test");

Route::get('menu', "DataController@menu")->name("ajx.menu");
Route::get('login/google', "DataController@createGoogleLoginUrl")->name("ajx.login.google");

Route::get('comic', "DataController@localComicList")->name("ajx.comic");
Route::get('comic/{comicId}', "DataController@localComic")->name("ajx.comic");
Route::get('ex/get', "DataController@ajxExList")->name("ajx.ex.get");
Route::post('ex/get', "DataController@ajxExList")->name("ajx.ex.get");
//Route::get('ex/get/img', "DataController@ajxExPage")->name("ajx.ex.get");
Route::post('ex/get/img', "DataController@ajxExPage")->name("ajx.ex.get");
