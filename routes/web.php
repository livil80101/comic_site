<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PageController@index")->name("root");
Route::get('comic/{comicId}', "PageController@comic")->name("comic.page")->where(["comicId" => "[0-9]+"]);
Route::get('login_by_google/result', "PageController@loginByGoogle")->name("login.google");
Route::get('test', "TestController@test");

Route::get('hentai', "PageController@exPage");

Auth::routes();

Route::middleware("auth")->group(function () {
    Route::get('aa', "PageController@exPage");
});

//針對全部路徑(已經廢棄)
//Route::get('/', "PageController@vue")->name("root");
//Route::get('{any}', "PageController@vue")->where(["any" => ".*"]);
//Auth::routes();

