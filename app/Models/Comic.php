<?php

namespace App\Models;

use App\Ext\Enum\Tables;
use Illuminate\Database\Eloquent\Model;

class Comic extends Model
{
    protected $table = Tables::COMIC;
}
