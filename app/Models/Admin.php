<?php

namespace App\Models;

use App\Ext\Enum\Tables;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * 這個應該不是我寫的，我猜是直接改編USER
 *
*/
class Admin extends Authenticatable
{
    public $table = Tables::ADMIN;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];
}
