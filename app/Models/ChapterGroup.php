<?php

namespace App\Models;

use App\Ext\Enum\Tables;
use Illuminate\Database\Eloquent\Model;

class ChapterGroup extends Model
{
    protected $table = Tables::CHAPTER_GROUP;
}
