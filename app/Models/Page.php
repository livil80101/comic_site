<?php

namespace App\Models;

use App\Ext\Enum\Tables;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = Tables::PAGE;
}
