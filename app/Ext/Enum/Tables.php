<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/10/27
 * Time: 下午8:01
 */

namespace App\Ext\Enum;

class Tables
{
    public const COMIC = "comic";
    public const CHAPTER_GROUP = "chapter_group";
    public const CHAPTER = "chapter";
    public const PAGE = "page";
    public const AUTHOR = "author";

    public const ADMIN = "admin";

}