<?php


namespace App\Repositories;


use App\Ext\Modules\ComicModule;
use App\Models\Comic;
use Illuminate\Http\Request;

class ComicRepository
{
    /**
     * 透過 Request 新增漫畫
     *
     * @param Request $request 輸入
     */
    public static function createComicByRequest(Request $request)
    {
        //新增漫畫
        $comic = new Comic();
        $comic->title = "";
        $comic->cover = "";
        $comic->author_name = "";
        $comic->author_id = "";
        $comic->description = '';
        $comic->views = 0;
        $comic->save();

        //新增章節
    }

    /**
     * 透過 Module 新增漫畫
     *
     */
    public static function createComicByModule()
    {

    }
}