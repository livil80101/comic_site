<?php

namespace App\Http\Controllers;

use App\Services\Account\AdminService;
use App\Services\ComicService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * @deprecated
 */
class PageController extends Controller
{
    //漫畫封面展示
    public function index()
    {
        dd("內部整修中");
        $me = auth()->user();
//        dd($me);
        return view("page.root", [
            "login" => createGoogleClient(),
            "all_comic" => ComicService::showComicList(),
            "me" => $me
        ]);
    }

    //單本漫畫
    public function comic($comicId)
    {
        $blade = "page.comic";

        $data = ComicService::showComic($comicId);

        return view($blade, [
            "images" => $data
        ]);
    }


    //以google登入
    public function loginByGoogle(Request $request)
    {
        $code = $request->input("code");

        $googleClient = createGoogleClient();

        $googleClient->fetchAccessTokenWithAuthCode($code);

//        Admin::getGoogleOAuthData($googleClient);

        $me = \App\Models\Admin::find(1);
        //這邊要補充null判定
        \Illuminate\Support\Facades\Auth::login($me);

        return redirect(route("root"));
    }

    //廢棄的
    public function exPage()
    {
        return view("page.hentai");
    }

    //廢棄的
    public function vue()
    {
        return view("page.vue");
    }
}
