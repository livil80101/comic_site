<?php

namespace App\Http\Controllers;

use App\Services\Account\AdminService;
use App\Services\ComicService;
use App\Services\File\LocationComicManager;
use App\Services\Spider\ExHentai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

/**
 * 這邊主要是用來生成ajax的
 *
 * @deprecated
 */
class DataController extends Controller
{

    //menu生成
    public function menu()
    {
        return Response::json(AdminService::getMenu());
    }

    public function createGoogleLoginUrl()
    {
        $admin = Auth::user();
        if ($admin) {
            $url = null;
        } else {
            $url = createGoogleClient()->createAuthUrl();
        }
        return Response::json([
            "url" => $url
        ], 200, [], JSON_UNESCAPED_SLASHES);
    }

    //本地端漫畫列表
    public function localComicList()
    {
        $comic = ComicService::showComicList();
        return Response::json([
            "comic" => $comic,
            "last" => $comic->lastPage(),
//            "link" => createLinks(1, $comic->currentPage(), $comic->lastPage())
        ]);


    }

    //本地端漫畫
    public function localComic($comicId)
    {
        $data = ComicService::showComic($comicId);
        return Response::json([
            "images" => $data
        ]);
    }

    //熊貓網的漫畫列表
    public function ajxExList(Request $request)
    {
        $url = $request->input("url", "https://exhentai.org/g/1322983/0bd2a1b5be/");

        $html = ExHentai::getPageContent($url);
        $list = ExHentai::getPageList($html);

        return Response::json($list, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function ajxExPage(Request $request)
    {
        $url = $request->input("url", "https://exhentai.org/s/ebd7411e3e/1322983-2");
        $html = ExHentai::getPageContent($url);
        $re = ExHentai::getImageUrl($html);

        return Response::json([
            "url" => $re,
            "input" => $request->all(),
        ], 200, [], JSON_UNESCAPED_SLASHES);

    }

}
