<?php

namespace App\Http\Controllers;

use App\Services\ComicService;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{

    public function page()
    {

    }

    //測試用
    public function test()
    {
//        $me = \App\Models\Admin::find(1);
//        Auth::login($me);

        $ffmpeg = FFMpeg::create();

        $video = $ffmpeg->open('video.mpg');

//        $video->

        dd(
            1
        );
        $data = ComicService::showComicList();
        return Response::json([
            "count" => $data->count(),
            "currentPage" => $data->currentPage(),
            "firstItem" => $data->firstItem(),
            "hasMorePages" => $data->hasMorePages(),
            "lastItem" => $data->lastItem(),
            "lastPage" => $data->lastPage(),
            "nextPageUrl" => $data->nextPageUrl(),
            "onFirstPage" => $data->onFirstPage(),
            "perPage" => $data->perPage(),
            "previousPageUrl" => $data->previousPageUrl(),
            "total" => $data->total(),
            "link" => createLinks(1, $data->currentPage(), $data->lastPage())
        ]);
//        return Response::api([]);
    }
}
