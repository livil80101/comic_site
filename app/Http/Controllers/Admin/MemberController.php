<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * 會員管理相關的controller
 *
 * 管理員登入(含google登入)
 *
 */
class MemberController extends Controller
{
    //登入
    public function login()
    {
        return view("login_by_admin");
    }

    //透過一般通道
    public function checkLogin()
    {

    }
}
