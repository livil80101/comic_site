<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/11/20
 * Time: 下午2:50
 */

function createGoogleClient()
{
    $googleClient = new \Google_Client();


    $googleClient->setApplicationName("login by gmail");

    $googleClient->setClientId(config("google.client_id"));
    $googleClient->setClientSecret(config("google.client_secret"));

    $googleClient->setScopes([
        \Google_Service_Plus::PLUS_LOGIN,
        \Google_Service_Plus::USERINFO_EMAIL,
    ]);

    $googleClient->setRedirectUri(config("google.redirect_uri"));

    return $googleClient;
}

function createLinks($per = 1, $current = 1, $last = 1, $option = [])
{
    $re = [
        "per" => $per,
        "last" => $last,
        "current" => $current,
        "main" => [],
        "dat3" => [
            "per" => false,
            "last" => false
        ],
    ];

    //針對首頁、末頁處理
    if ($current == $per) {
        $current++;
    } else if ($current == $last) {
        $current--;
    }

    $limit = @$option["limit"] ? $option["limit"] : 8;
    $option_per = @$option["per"] ? $option["per"] : 5;
    $option_last = @$option["last"] ? $option["last"] : 4;

    $target = $current;

    for ($i = 0; $i < $limit / 2; $i++) {
        if ($target - 1 <= $per) {
            break;
        }
        $target--;
    }
    for ($i = 0; $i < $limit; $i++) {
        if ($target + $i >= $last) {
            break;
        }
        $re["main"][] = $target + $i;
    }


    //判定 ...
    if (($current - $per) > $option_per) {
        $re["dat3"]["per"] = true;
    }
    if (($last - $current) > $option_last) {
        $re["dat3"]["last"] = true;
    }
//        dd($re["main"]);
    return $re;
}