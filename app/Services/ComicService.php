<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/10/27
 * Time: 下午11:07
 */

namespace App\Services;


use App\Models\Page;
use Illuminate\Support\Facades\DB;

class ComicService
{
    public static function showComicList($page = 0)
    {
        /**
         *
         */
        return Page::where("priority", 0)
            ->join("comic", "comic.id", "=", "page.comic_id")
            ->select(
                "comic.id",
                "comic.title",
                "page.url",
                "comic.remark",
                "comic.created_at",
                "comic.updated_at"
            )
            ->paginate(5);
//            ->toSql();
    }

    /**
     * @param int|string $comicId 漫畫編號
     *
     * @return mixed
     */
    public static function showComic($comicId)
    {
        return Page::join("comic", "comic.id", "=", "page.comic_id")
            ->where("comic.id", $comicId)
            ->get();
    }

    private $temp = <<<SQL
select 
*
from
comic,
	(
	select
	 id,
	 comic_id ,
	 min(page.`priority`)
	from
	 page 
	group by
	 `comic_id`
	) as cover,
	page 
where
 cover.comic_id = comic.id
 and page.id = cover.comic_id
SQL;


}