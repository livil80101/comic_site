<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/11/20
 * Time: 上午11:44
 */

namespace App\Services\Account;

use App\Models\Admin as ModelAdmin;
use Illuminate\Support\Facades\Auth;

class AdminService
{
    /**
     * 使用token兌換使用者資料
     *
     * @param \Google_Client $client 客戶端
     *
     * @return bool
     */
    public static function getGoogleOAuthData(\Google_Client $client)
    {
        $OAuth = new \Google_Service_Oauth2($client);

        $userData = $OAuth->userinfo_v2_me->get();

        $email = $userData->getEmail();

        self::login($email);
    }

    public static function login($email)
    {
        if (ModelAdmin::count() < 1) {
            $model = new ModelAdmin();
            $model->email = config("google.master");
            $model->save();
        }

        $admin = ModelAdmin::where("email", $email)
            ->first();

        if (empty($admin)) {
            return;
        }
        Auth::login($admin);
    }

}