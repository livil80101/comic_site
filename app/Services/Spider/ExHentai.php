<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/11/16
 * Time: 上午11:18
 */

namespace App\Services\Spider;


use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class ExHentai
{
    /**
     * 展示用
     */
    public static function show()
    {
        //取得單頁
        $html = ExHentai::getPageContent("https://exhentai.org/g/1315405/2d593bcb92");
        //在單頁有取得東西的狀況，取得圖片列表
        if (empty($html)) return;
        $data = ExHentai::getPageList($html);
        //展開列表，並取得圖片url
        foreach ($data["list"] as $item) {
            $img = ExHentai::getImageUrl(
                ExHentai::getPageContent($item["href"])
            );
            echo "<img src='" . $img . "''>";
            dd(
                $img
            );
        }
    }

    /**
     * 取得單頁資料
     *
     * @param string $url
     *
     * @throws
     *
     * @return string
     */
    public static function getPageContent($url)
    {
        $client = new Client();
        $body = $client->request("GET", $url, [
            'headers' => [
                'Cookie' => static::getCookie()
            ]
        ])->getBody();

        try {
            $re = $body->getContents();
        } catch (\Exception $ex) {
            return null;
        }

        return $re;
    }

    /**
     * 將表單分離出來，並取得連結與css
     *
     * @param string $html 網頁內容
     *
     * @return array
     */
    public static function getPageList($html)
    {
        $crawler = new Crawler($html);

        $title = $crawler->filter("#gn")->text();
        $subTitle = $crawler->filter("#gj")->text();

        $pages = $crawler->filter("table.ptt td:nth-last-child(2) a")->text();
        $now = $crawler->filter("table.ptt td.ptds a")->text();

        $tables = $crawler->filter('.gdtm div')->each(function (Crawler $node, $i) {
            $css = $node->attr("style");
            $href = $node->filter("a")->attr("href");

            return [
//                "html" => $node->html(),
                "css" => $css,
                "href" => $href
            ];
        });

        return [
            "title" => $title,
            "sub_title" => $title,
            "pages" => $pages,
            "now" => $now,
            "list" => $tables
        ];
    }

    public static function getImageUrl($html)
    {
        $crawler = new Crawler($html);
        $pages = $crawler->filter("img#img")->attr("src");

        return $pages;
    }


    /**
     * 取得一行式的cookie
     *
     * @return string
     */
    public static function getCookie()
    {
        return "igneous=" . config("hero.igneous") . ";"
            . "ipb_member_id=" . config("hero.ipb_member_id") . ";"
            . "ipb_pass_hash=" . config("hero.ipb_pass_hash") . ";"
            . "lv=" . config("hero.lv") . ";"
            . "s=" . config("hero.s") . ";"
            . "sk=" . config("hero.sk") . ";"
            . "uconfig=" . config("hero.uconfig") . ";";
    }
}