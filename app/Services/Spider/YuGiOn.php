<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/11/15
 * Time: 下午6:17
 */

namespace App\Services\Spider;


use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class YuGiOn
{
    /**
     * 取得單頁資料
     *
     * @param string $url
     *
     * @throws
     *
     * @return string
     */
    public static function getPageContent($url)
    {
        $client = new Client();
        $body = $client->request("GET", $url)->getBody();

        try {
            $re = $body->getContents();
        } catch (\Exception $ex) {
            return null;
        }

        return $re;
    }

    /**
     * 解析卡片頁面
     *
     * @param string $html
     *
     *
     */
    public static function divideCard($html)
    {
        $crawler = new Crawler($html);

        $tables = $crawler->filter('.Hover.w2')->each(function (Crawler $node, $i) {
            return $node->html();
        });

        dd(
            $tables
        );
    }

    /**
     * 解析卡片分類
     *
     * @param array $tables 欲解析列表
     *
     */
    public static function divideCardTable(array $tables)
    {
        //先解析第一行，看是對自己還是對敵人


    }
}