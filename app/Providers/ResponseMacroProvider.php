<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro("api", function ($data = null, $httpStatus = 200, array $header = [], $option = 0) {
            return Response::json([
                "data" => $data,
                "service_time" => Carbon::now(),
                "code" => $httpStatus
            ], $httpStatus, $header, $option);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
