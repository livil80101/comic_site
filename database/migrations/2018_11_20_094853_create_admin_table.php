<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Ext\Enum\Tables::ADMIN, function (Blueprint $table) {
            $table->increments('id');
            $table->string("account", 50)->nullable();
            $table->string("password", 50)->nullable();
            $table->string("email", 50)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\App\Ext\Enum\Tables::ADMIN);
    }
}
