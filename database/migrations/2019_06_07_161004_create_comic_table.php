<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Ext\Enum\Tables::COMIC, function (Blueprint $table) {
            $table->increments('id')->comment("id");
            $table->string("title", 30)->comment("標題");
            $table->string("cover")->comment("封面");
            $table->string("author_name", 30)->comment("作者名稱	");
            $table->unsignedInteger("author_id")->comment("作者編號	");
            $table->text("description")->comment("介紹");
            $table->boolean("visible")->comment("是否遮蔽")->default(false);
            $table->unsignedInteger("views")->comment("人氣");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\App\Ext\Enum\Tables::COMIC);
    }
}
