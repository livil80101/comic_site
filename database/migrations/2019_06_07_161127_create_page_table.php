<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Ext\Enum\Tables::PAGE, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("ch_id")->comment("章節編號");
            $table->unsignedInteger("priority")->comment("章節編號");
            $table->string("path",255)->comment("圖片路徑");
            $table->string("preview",255)->comment("預覽圖片路徑");
            $table->boolean("visible")->comment("是否遮蔽")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\App\Ext\Enum\Tables::PAGE);
    }
}
