<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChapterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Ext\Enum\Tables::CHAPTER, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("cg_id")->comment("群組編號");
            $table->unsignedInteger("c_id")->comment("漫畫編號");
            $table->string("title",30)->comment("標題");
            $table->timestamp("open_time")->comment("發佈時間");
            $table->boolean("visible")->comment("是否遮蔽")->default(0)->default(false);
            $table->string("source")->comment("來源")->nullable();
            $table->unsignedTinyInteger("source_type")->comment("來源類型");
            $table->unsignedTinyInteger("priority")->comment("排序");
            $table->unsignedTinyInteger("previous")->comment("上一話")->nullable();
            $table->unsignedTinyInteger("next")->comment("下一話")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\App\Ext\Enum\Tables::CHAPTER);
    }
}
