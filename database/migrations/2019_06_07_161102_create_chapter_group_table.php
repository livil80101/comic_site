<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChapterGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Ext\Enum\Tables::CHAPTER_GROUP, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("c_id")->comment("漫畫編號");
            $table->string("title" , 30)->comment("群組標題");
            $table->boolean("visible")->comment("是否遮蔽")->default(false);
            $table->unsignedInteger("priority")->comment("排序");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(\App\Ext\Enum\Tables::CHAPTER_GROUP);
    }
}
