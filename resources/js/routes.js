import VueRouter from "vue-router";
import Home from './views/Home';
import Hello from './views/List';

let route = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/list',
            name: 'list',
            component: Hello,
        },
        {
            path: '/hello/:id',
            name: 'hello',
            component: Hello,
        },

    ],
});

export default route;