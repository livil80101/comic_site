/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// console.log("有執行 bootstrap ");
// require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

import Vue from 'vue';
import VueRouter from "vue-router";

import axios from 'axios';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios);

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter);
Vue.use(BootstrapVue);

import "./importComponents";

// import Test from './components/Test';
// Vue.component("test", Test);

import App from './views/App';
import Home from './views/Home';
import List from './views/List';
import Comic from './views/Comic';
import Hentai from './views/Hentai';

// const router = require("./routes");

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/list',
            name: 'list',
            component: List,
        },
        {
            path: '/c/:id',
            name: 'comic',
            component: Comic,
        }
    ],
});

const app = new Vue({
    el: "#app",
    components: {App},
    router
});

