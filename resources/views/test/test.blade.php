@extends("layouts.element")

@section("content")
    <p>喵</p>
    <button @click="bar">GO GO</button>
@endsection



@section("before_js")
    <script>
        var ext = {
            data: {
                code: "喵",

            },
            methods: {
                bar: function () {
                    // triggers when I click the bar button
                    console.log('bar');
                    console.log(this.name); // John
                    console.log(this.code); // John
                },

            }
        };
    </script>
@endsection

@section("js")

@endsection

@section("css")
    <style>
        .el-menu-vertical-demo:not(.el-menu--collapse) {
            width: 200px;
            min-height: 400px;
        }
    </style>
@endsection