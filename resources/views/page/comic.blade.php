@extends("layouts.default")

@section("content")
    @foreach($images as $img)
        <div class="show">
            <img class="comic" src="{{ $img->url }}">
        </div>
    @endforeach
@endsection

@section("ex_script")

@endsection

@section("ex_style")
    <style>
        .show {
            width: 100%;
            text-align: center;
        }

        .comic {
            max-width: 85%;
        }
    </style>
@endsection
