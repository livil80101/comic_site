@extends("layouts.default")

@section("content")


    <div class="container">
        <header class="blog-header py-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4 pt-1">

                </div>
                <div class="col-4 text-center">
                    <a class="blog-header-logo text-dark" href="#">天譴</a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    @if($me)
                        <a class="btn btn-danger btn-sm " href="">進入管理</a>
                    @else
                        <a class="btn btn-success btn-sm " href="{{ $login->createAuthUrl() }}">登入</a>
                    @endif
                </div>
            </div>
        </header>

        {{-- menu --}}
        {{--<div class="nav-scroller py-1 mb-2">--}}
        {{--<nav class="nav d-flex justify-content-between">--}}
        {{--<a class="p-2 text-white" href="#">World</a>--}}
        {{--<a class="p-2 text-white" href="#">U.S.</a>--}}
        {{--</nav>--}}
        {{--</div>--}}

        {{--輪撥--}}
        {{--<div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">--}}
        {{--<div class="col-md-6 px-0">--}}
        {{--<h1 class="display-4 font-italic">Title of a longer featured blog post</h1>--}}
        {{--<p class="lead my-3">Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.</p>--}}
        {{--<p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="row mb-2">
            @foreach($all_comic as $comic)
                <div class="col-md-12">
                    <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                        <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-primary">用過</strong>
                            <h3 class="mb-0">
                                <a class="text-dark" href="{{ route("comic.page",$comic->id) }}">{{ $comic->title }}</a>
                            </h3>
                            <div class="mb-1 text-muted">Nov 12</div>
                            <p class="card-text mb-auto">{{ $comic->remark }}</p>
                            <a href="{{ route("comic.page",$comic->id) }}">進入</a>
                        </div>
                        <img class="card-img-right flex-auto" src="{{ $comic->url }}" alt="這邊是封面">
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row mb-2">
            {{ $all_comic->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>

@endsection

@section("ex_script")

@endsection

@section("ex_style")
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <style>
        body {
            background-color: grey;
        }

        /* stylelint-disable selector-list-comma-newline-after */

        .blog-header {
            line-height: 1;
            border-bottom: 1px solid #e5e5e5;
        }

        .blog-header-logo {
            font-family: "Playfair Display", Georgia, "Times New Roman", serif;
            font-size: 2.25rem;
        }

        .blog-header-logo:hover {
            text-decoration: none;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "Playfair Display", Georgia, "Times New Roman", serif;
        }

        .display-4 {
            font-size: 2.5rem;
        }

        @media (min-width: 768px) {
            .display-4 {
                font-size: 3rem;
            }
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }

        .nav-scroller .nav-link {
            padding-top: .75rem;
            padding-bottom: .75rem;
            font-size: .875rem;
        }

        .card-img-right {
            height: 100%;
            border-radius: 0 3px 3px 0;
        }

        .flex-auto {
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .h-250 {
            height: 250px;
        }

        @media (min-width: 768px) {
            .h-md-250 {
                height: 250px;
            }
        }

        /*
         * Blog name and description
         */
        .blog-title {
            margin-bottom: 0;
            font-size: 2rem;
            font-weight: 400;
        }

        .blog-description {
            font-size: 1.1rem;
            color: #999;
        }

        @media (min-width: 40em) {
            .blog-title {
                font-size: 3.5rem;
            }
        }

        /* Pagination */
        .blog-pagination {
            margin-bottom: 4rem;
        }

        .blog-pagination > .btn {
            border-radius: 2rem;
        }

        /*
         * Blog posts
         */
        .blog-post {
            margin-bottom: 4rem;
        }

        .blog-post-title {
            margin-bottom: .25rem;
            font-size: 2.5rem;
        }

        .blog-post-meta {
            margin-bottom: 1.25rem;
            color: #999;
        }

        /*
         * Footer
         */
        .blog-footer {
            padding: 2.5rem 0;
            color: #999;
            text-align: center;
            background-color: #f9f9f9;
            border-top: .05rem solid #e5e5e5;
        }

        .blog-footer p:last-child {
            margin-bottom: 0;
        }
    </style>
@endsection