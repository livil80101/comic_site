<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js"></script>
    <script src="/plugin/element.js"></script>

    <title>後台管理系統</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/plugin/element.css">
    <style>
        .el-header {
            background-color: #B3C0D1;
            color: #333;
            line-height: 60px;
        }

        .el-aside {
            color: #333;
        }

        .el-main {
            padding: 5px;
        }

        [v-cloak] {
            display: none;
        }
    </style>
    @yield("css")
</head>
<body>
<div id="app" v-cloak>

    <el-container style="height: 100%; border: 1px solid #eee">

        @include("layouts.element_menu")

        <el-container>
            <el-header style="text-align: right; font-size: 12px">
                <el-dropdown>
                    <i class="el-icon-setting" style="margin-right: 15px"></i>
                    <el-dropdown-menu slot="dropdown">
                        <el-dropdown-item>查看</el-dropdown-item>
                        <el-dropdown-item>新增</el-dropdown-item>
                        <el-dropdown-item>删除</el-dropdown-item>
                    </el-dropdown-menu>
                </el-dropdown>
                <span>王小虎</span>
            </el-header>

            <el-main>

                @yield("content")

            </el-main>
        </el-container>
    </el-container>

</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

@yield("before_js")
<script>
    const app = new Vue({
        el: '#app',
        data: {
            name: 'John',
            isCollapse: false,
        },
        mixins: [ext],
        methods: {
            gotoPage(even) {
                let target = even.$attrs.target;
                console.log(target);
                location.href = target;
            }
        }
    });
</script>
@yield("js")
</body>
</html>