# 漫畫

## comic

漫畫，就是進入漫畫的介紹頁面

作者部分考慮到同作者不同名連載的狀況（比方說金田一），所以這邊給他一個手動填

這邊遮蔽的話，使用者會看到404

如果是 血腥、色情，要從 tags 那邊關聯

人氣部分想要用redis記錄一天的資訊後再寫入，避免過度頻繁訪問資料表

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id         |INT(11)        ||id，自動編號|key,<br>auto_increment,<br>unsigned|
|title      |VARCHAR(30)    ||標題 ||
|cover      |VARCHAR(191)   ||封面 ||
|author_name|VARCHAR(30)    ||作者名稱 ||
|author_id  |INT(11)        ||作者編號 |unsigned |
|description|TEXT           ||介紹 ||
|visible    |TINYINT(4)     |0|是否遮蔽 ||
|views      |INT(11)        ||人氣 ||
|created_at |TIMESTAMP      |null|創建時間|allow null|
|updated_at |TIMESTAMP      |null|修改時間|allow null|

## chapter_group

章節群組

比方說單行本版 跟 連載版 就要用這個來區分

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id         |INT(11)    ||id，自動編號 |key,<br>auto_increment,<br>unsigned|
|c_id       |INT(11)    ||漫畫編號 |unsigned |
|title      |VARCHAR(30)||群組標題 ||
|visible    |TINYINT(4) |0|是否遮蔽 ||
|priority   |INT(11)    ||排序 |unsigned|
|created_at |TIMESTAMP  |null|創建時間|allow null|
|updated_at |TIMESTAMP  |null|修改時間|allow null|

## chapter

章節，基本上是核心部分

為了能更快速取得隸屬漫畫，所以這邊同時有 c_id 跟 cg_id

open_time 是漫畫發布時間，可以選擇未來的時間發布，這邊要配合 Even 跟 Queue

目前來源只有 Ex 那邊，類型目前有 手動、EX 兩種 

上一話、下一話如果對到隱藏的章節就直接給下一話

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id             |INT(11)        ||id，自動編號 |key,<br>auto_increment,<br>unsigned|
|cg_id          |INT(11)        ||群組編號 |unsigned |
|c_id           |INT(11)        ||漫畫編號 |unsigned |
|title          |VARCHAR(30)    ||標題 | |
|open_time      |TIMESTAMP      ||發佈時間 | |
|visible        |TINYINT(4)     |0|是否遮蔽 | |
|source         |VARCHAR(191)   ||來源 |allow null |
|source_type    |TINYINT(4)     ||來源類型 |unsigned |
|priority       |INT(11)        ||排序 |unsigned |
|previous       |INT(11)        ||上一話 |unsigned <br>allow null|
|next           |INT(11)        ||下一話 |unsigned <br>allow null|
|created_at     |TIMESTAMP      |null|創建時間|allow null|
|updated_at     |TIMESTAMP      |null|修改時間|allow null|

## page

漫畫頁，用來標記檔案的位置

當檔案上傳時，會生成一個比較小的預覽圖避免流量爆表

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id             |INT(11)        ||id，自動編號|key,<br>auto_increment,<br>unsigned|
|ch_id          |INT(11)        ||章節編號 |unsigned |
|priority       |INT(11)        ||排序 |unsigned |
|path           |VARCHAR(255)   ||圖片路徑 | |
|preview        |VARCHAR(255)   ||預覽圖片路徑 | |
|visible        |TINYINT(4)     |0|是否遮蔽 | |
|created_at     |TIMESTAMP      |null|創建時間|allow null|
|updated_at     |TIMESTAMP      |null|修改時間|allow null|

## author

作者編號

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id         |INT(11)        ||id，自動編號|key,<br>auto_increment,<br>unsigned|
|name       |VARCHAR(10)    ||作者名稱||
|created_at |TIMESTAMP      |null|創建時間|allow null|
|updated_at |TIMESTAMP      |null|修改時間|allow null|

# 管理員

## admin

用於登入的

一般來說只有一人，但是為了配合 Auth ，所以要挖一個資料表

|column |type   |default    |description    |other  |
|---|---|---|---|---|
|id             |INT(11)        ||id，自動編號|key,<br>auto_increment,<br>unsigned|
|email          |VARCHAR(50)    ||E-mail||
|remember_token |VARCHAR(100)   ||識別碼|allow null|
|created_at     |TIMESTAMP      |null|創建時間|allow null|
|updated_at     |TIMESTAMP      |null|修改時間|allow null|


# 保留（empty）

## 保留資料表

|column |type   |default    |description    |other  |
|---|---|---|---|---|
||||||
