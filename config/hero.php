<?php
return [
    "domain"=>"exhentai.org",
    "igneous" => env("HERO_IGNEOUS", ""),
    "ipb_member_id" => env("HERO_IPB_MEMBER_ID", ""),
    "ipb_pass_hash" => env("HERO_IPB_PASS_HASH", ""),
    "lv" => env("HERO_LV", ""),
    "s" => env("HERO_S", ""),
    "sk" => env("HERO_SK", ""),
    "uconfig" => env("HERO_UCONFIG", ""),
];