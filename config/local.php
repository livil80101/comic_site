<?php
/**
 * Created by PhpStorm.
 * User: Weil
 * Date: 2018/10/27
 * Time: 下午9:10
 */
return [
    "comic_path" => env("IMAGE_PREVIEW_DIR"),
    "comic_url" => env("IMAGE_PREVIEW_URL"),
];