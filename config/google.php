<?php

return [
    "client_id" => env("GOOGLE_OAUTH_CLIENT_ID"),
    "client_secret" => env("GOOGLE_OAUTH_CLIENT_SECRET"),
    "key_path" => env("GOOGLE_KEY_PATH"),
    "password" => env("GOOGLE_KEY_PW"),
    //
    "master" => env("MASTER_EMAIL"),
    "redirect_uri" => env("GOOGLE_REDIRECT_URI"),
];